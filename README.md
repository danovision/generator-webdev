# generator-webdev

## Installation

First install [Yeoman](http://yeoman.io).

```
$ npm install -g yo
```

Currently not published on npm. For now, clone this repo and

```
$ cd /path/to/repo/
$ npm link
```

## Usage

Make a new folder, cd to it, then initiate the generator:

```
$ mkdir newproject && cd $_
$ yo webdev
```

## Update subtree project

```
$ git pull -s subtree app/templates master
```


## License

MIT
